elements_groupe(n) =
{
	/*
		Retourne le vecteur de tous les éléments du groupe Zn* pour un n donné
	*/
	
	groupe = [1]; \\ on initialise le vecteur à 1
	
	for(a = 2, n - 1, \\ on commence à 2 comme on a déjà 1 dans le vecteur
		if(gcd(a,n) == 1, \\ si a appartient au groupe Zn*
			tmp = [a];
			groupe = concat(groupe,tmp); \\ on rajoute la valeur de a dans le vecteur du groupe
		);
	);
	
	return (groupe);
}

ordre_multiplicatif(a, n) =
{
	/*
		Retourne l'ordre multiplicatif de a modulo n
		valeur du plus petit entier k tel que a^k congruent à 1 modulo n
		avec a un element du groupe Zn*
	*/
	
	for(k = 1, 1000,
		if(a ^ k % n == 1, \\ si a^k est congruent à 1 modulo n
			return (k);
		);
	);
			
	return (0);
}

racine_primitive(groupe, n) =
{
	/*
		Retourne la racine primitive du groupe Zn*
		valeur de l'element du groupe Zn* pour lequel l'ordre multiplicatif modulo n est égal à phi(n)
	*/
	
	for(i = 1, #groupe, \\ pour tous les elements du groupe
		if(ordre_multiplicatif(groupe[i], n) == eulerphi(n), \\ si l'ordre multiplicatif de a modulo n est egal à phi(n)
			return (groupe[i]);
		);
	);
}

est_cyclique(n) =
{
	/*
		Retourne si un groupe Zn* est cyclique ou non
		si c'est le cas il donne un générateur de ce groupe et démontre pourquoi
	*/
	
	groupe = elements_groupe(n); \\ on remplie le vecteur contenant les valeurs des élements du groupe Zn*
	generateur = racine_primitive(groupe,n); \\ on trouve le générateur du groupe Zn*
	
	print ("Le groupe Z"n"* est composé des elements suivants : " groupe "\n");
	
	if (generateur > 0, \\ si on trouve un générateur du groupe Zn*
		printf("Z" n "* est un groupe cyclique et " generateur " génère ce groupe\n");
		
		/*
			Pour prouver que c'est bien le générateur on va générer tous les valeurs du groupe en mettant le générateur aux puissances entre 0 et phi(n)-1
		*/
		
		if (n <= 30, \\ si n<=30 on apporte la preuve que c'est bien le générateur
			printf("En voici la preuve :\n");
			for(i=0, eulerphi(n)-1, \\ pour tous les a^i tel que i = 0 ... phi(n) avec a generateur du groupe Zn*
				printf(generateur "^" i " = " generateur^i%n "\n");
			);
		);
		
		, \\ ELSE
		printf("Z" n "* n'est pas un groupe cyclique");
	);
}
