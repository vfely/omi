probabilite(nb_faces, val_mini) =
{
	/*
		Calcul de la probabilité d'obtenir au moins la valeur val_mini dans un dé de nb_faces
	*/
	
	return (((nb_faces - val_mini + 1) / nb_faces) * 1.0);
}

esperance(nb_faces, val_mini) =
{
	/*
		Calcul du nombre moyen de lancers nécessaires pour obtenir au moins la valeur val_mini sur un dé à nb_faces
	*/
	
	return ((1 / probabilite(nb_faces, val_mini)) * 1.0);
}

experimental(nb_faces, val_mini) =
{
	/*
		Simulation du lancer d'un dé à nb_faces
		Retourne le nombre de lancers effectués jusqu'à obtenir au moins la valeur val_mini
	*/
	
	nb_lancers = 0;

	for(i = 1, 1000,
		nb_lancers++;
		alea = random(nb_faces) + 1; \\ génère un nombre entre 1 et nb_faces, simule le lancer du dé
		
		if(alea >= val_mini, \\ si on tire au moins la valeur val_mini
			return (nb_lancers);
		);
	);
}

realisation(nb_exp, nb_faces, val_mini) =
{
	/*
		Simulation de nb_exp experiences de lancers de dé
		Retourne la valeur moyenne du nombre de lancers effectués jusqu'à obtenir au moins la valeur val_mini
	*/
	
	somme = 0;
	
	for(i = 1, nb_exp,
		somme += experimental(nb_faces, val_mini);
		moyenne = somme / i * 1.0;
	);
	
	return (moyenne);
}

probabilite_degats(val_mini2, nb_faces1, val_mini1, nb_faces2) =
{
	/*
		Retourne la probabilité d'obtenir au moins val_mini2 sur un dé à nb_faces2 faces
		sachant qu'on a obtenu au moins val_mini1 sur un dé à nb_faces1 faces
	*/
	
	return (probabilite(nb_faces1, val_mini1) * probabilite(nb_faces2, val_mini2));
}

esperance_degats(val_mini2, nb_faces1, val_mini1, nb_faces2) =
{
	/*
		Calcul du nombre moyen de lancers nécessaires pour obtenir au moins la valeur val_mini2 sur un dé à nb_faces2
		sachant qu'on a obtenu au moins val_mini1 sur un dé à nb_faces1
	*/
	
	return (1 / probabilite_degats(val_mini2, nb_faces1, val_mini1, nb_faces2));
}

experimental_degats(val_mini2, nb_faces1, val_mini1, nb_faces2) =
{
	/*
		Simulation du lancer de deux dés, le premier à nb_faces1 et le second à nb_faces2
		Retourne le nombre de lancers effectués jusqu'à obtenir au moins la valeur val_mini2 sur le second dé
	*/
	
	nb_lancers = 0;
	nb_degats = 0;
	
	for(i=1, 1000,
		nb_lancers++;
		alea = random(nb_faces1) + 1; \\ génère un nombre entre 1 et nb_faces1, simule le lancer du premier dé
		
		if(alea >= val_mini1, \\ si on tire au moins la valeur val_mini1 on peut lancer le second dé
			nb_degats = random(nb_faces2) + 1; \\ génère un nombre entre 1 et nb_faces2, simule le lancer du second dé
			
			if(nb_degats >= val_mini2, \\ si on tire au moins la valeur val_mini2
				return (nb_lancers);
			);
		);
	);
}

realisation_degats(nb_exp, val_mini2, nb_faces1, val_mini1, nb_faces2) =
{
	
	/*
		Simulation de nb_exp experiences de lancers de dé
		Retourne la valeur moyenne du nombre de lancers effectués jusqu'à obtenir au moins la valeur val_mini
	*/	
	
	somme = 0;
	
	for(i = 1, nb_exp,
		somme += experimental_degats(val_mini2, nb_faces1, val_mini1, nb_faces2);
		moyenne = somme / i * 1.0;
	);
	
	return (moyenne);
}

experimental_degats_cumul(val_mini2, nb_faces1, val_mini1, nb_faces2) =
{
	/*
		Simulation du lancer de deux dés, le premier à nb_faces1 et le second à nb_faces2
		Retourne le nombre de lancers effectués jusqu'à ce que les valeurs cumulés sur le second dé atteignent au moins val_mini2
	*/
	
	nb_lancers = 0;
	nb_degats = 0;
	
	for(i = 1, 1000,
		nb_lancers++;
		alea = random(nb_faces1) + 1; \\ génère un nombre entre 1 et nb_faces1, simule le lancer du premier dé
		
		if(alea >= val_mini1, \\ si on obtient au moins val_mini1 sur le premier dé
			nb_degats += random(nb_faces2) + 1; \\ génère un nombre entre 1 et nb_faces2, simule le lancer du second dé (incrémentation de nb_degats)
		);
		
		if(nb_degats >= val_mini2, \\ si on cumule au moins nb_degats
			return (nb_lancers);
		);
	);
}

realisation_degats_cumul(nb_exp, val_mini2, nb_faces1, val_mini1, nb_faces2) =
{
	/*
		Simulation de nb_exp experiences de lancers de dé
		Retourne la valeur moyenne du nombre de lancers effectués jusqu'à cumuler au moins val_mini2 avec le second dé
	*/
	
	somme = 0;
	
	for(i = 1, nb_exp,
		somme += experimental_degats_cumul(val_mini2, nb_faces1, val_mini1, nb_faces2);
		moyenne = somme / i * 1.0;
	);
	
	return (moyenne);
}

exemples(nb_exp) =
{
	print ("Pour un archer expérimenté :\n\n");
	nb_faces1 = 20;
	val_mini1 = 4;
	
	print (" * Probabilité qu'il atteigne sa cible : " probabilite(nb_faces1, val_mini1));
	print ("   (probabilité d'obtenir au moins 4 avec un dé-20)");
	print (" * En théorie, il atteindra sa cible au bout de " esperance(nb_faces1, val_mini1) " lancers");
	print ("   (esperance du nombre de lancers pour obtenir au moins 4 avec un dé-20)");
	print (" * Après simulation de " nb_exp " expériences, c'est en moyenne au bout de " realisation(nb_exp, nb_faces1, val_mini1) " lancers");
	
	nb_faces2 = 4;
	val_mini2 = 4;
	print (" * En théorie, il infligera 4 points de dégats avec un arc de mauvaise facture en " esperance_degats(val_mini2,nb_faces1,val_mini1,nb_faces2) " flèches");
	print ("   (esperance du nombre de lancers pour obtenir au moins 4 avec un dé-4 sachant qu'on a obtenu au moins 4 avec un dé-20)");
	print (" * Après simulation de " nb_exp " expériences il inflige 4 points de dégats avec ce même arc en " realisation_degats(nb_exp,val_mini2,nb_faces1,val_mini1,nb_faces2) " flèches");
	
	val_mini2 = 100;
	print (" * Après simulation de " nb_exp " expériences il détruit le mannequin avec un arc de mauvaise facture en " realisation_degats_cumul(nb_exp,val_mini2,nb_faces1,val_mini1,nb_faces2) " flèches");
	
	/* -----------*/
	
	print ("\n\nPour un archer peu expérimenté :\n\n");
	nb_faces1 = 20;
	val_mini1 = 18;

	print (" * Probabilité qu'il atteigne sa cible : " probabilite(nb_faces1, val_mini1));
	print ("   (probabilité d'obtenir au moins 18 avec un dé-20)");
	print (" * En théorie, il atteindra sa cible au bout de " esperance(nb_faces1, val_mini1) " lancers");
	print ("   (esperance du nombre de lancers pour obtenir au moins 18 avec un dé-20)");
	print (" * Après simulation de " nb_exp " expériences, c'est en moyenne au bout de " realisation(nb_exp, nb_faces1, val_mini1) " lancers");
	
	nb_faces2 = 12;
	val_mini2 = 4;
	print (" * En théorie, il infligera 4 points de dégats avec un arc de bonne facture en " esperance_degats(val_mini2,nb_faces1,val_mini1,nb_faces2) " flèches");
	print ("   (esperance du nombre de lancers pour obtenir au moins 4 avec un dé-12 sachant qu'on a obtenu au moins 18 avec un dé-20)");
	print (" * Après simulation de " nb_exp " expériences il inflige 4 points de dégats avec ce même arc en " realisation_degats(nb_exp,val_mini2,nb_faces1,val_mini1,nb_faces2) " flèches");

	val_mini2 = 100;
	print (" * Après simulation de " nb_exp " expériences il détruit le mannequin avec un arc de bonne facture en " realisation_degats_cumul(nb_exp,val_mini2,nb_faces1,val_mini1,nb_faces2) " flèches");
}
