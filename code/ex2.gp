phi_euler(n) =
{	
	/*
		Calcul de la valeur de phi d'Euler pour un n donné
	*/
	
	produit = 1;
	
	mat = factor(n); \\ on remplie une matrice avec les facteurs premiers qui composent n
	taille_mat = matsize(mat); \\ on récupère la taille de cette matrice -> [nb_lignes,nb_colonnes]
	nb_lignes = taille_mat[1]; \\ la première valeur de taille_mat est le nombre de lignes
	
	for(i = 1, nb_lignes,
		\\ mat[i, 1] vaut le nombre premier
		\\ mat[i, 2] vaut le nombre de fois qu'on trouve ce nombre premier dans la liste des facteurs premiers (donc son exposant)
		\\ pour chaque ligne on factorise le nombre exposant -1 par lui même -1
		produit *= (mat[i, 1] ^ (mat[i, 2]-1)) * (mat[i, 1] - 1); \\ on factorise cette valeur par la valeur du facteur obtenue precedemment
	);
	
	print ("phi(" n ") = " produit);
}
